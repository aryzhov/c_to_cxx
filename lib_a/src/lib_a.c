#include "lib_a.h"

#include <stdio.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif

void FunctionFromLibA()
{
    printf("Say hellow to my little friend");
}

#ifdef __cplusplus
}
#endif
