#include <lib_a.h>
#include <lib_b.hpp>


int main()
{
    FunctionFromLibA();
    shared_lib::CallFunctionFromLibA();
    shared_lib::FunctionFromLibB();

    return 0;
}
