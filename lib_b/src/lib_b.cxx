#include "lib_b.hpp"

#include <iostream>

extern "C" {
#include <lib_a.h>
}

namespace shared_lib {
void CallFunctionFromLibA() {
  std::cout << "Call function " << __func__ << std::endl;

  FunctionFromLibA();
}

void FunctionFromLibB() {
  std::cout << "Call function " << __func__ << std::endl;
}
} // namespace shared_lib
