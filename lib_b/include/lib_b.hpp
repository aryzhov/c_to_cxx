#pragma once

#ifdef _MSC_VER
#define B_EXPORT __declspec(dllexport)
#else
#define B_EXPORT
#endif

namespace shared_lib
{
    B_EXPORT void CallFunctionFromLibA();

    B_EXPORT void FunctionFromLibB();

}