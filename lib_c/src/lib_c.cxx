#include "lib_c.hpp"

#include <iostream>

#include <lib_b.hpp>

namespace shared_lib {

void CallFunctionFromLibB() {
  
  FunctionFromLibB();
  

  std::cout << "Call function " << __func__ << std::endl;
}
} // namespace shared_lib
